/*****************************************************************
Auteurs : ASSE Romain
Date : 21 novembre 2022
Fichier : outils.cxx
But : définir les actions seuillage et différence
*****************************************************************/

#include <stdlib.h>
#include "outils.h"
#include "image.h"

/**
 * Permet d'effectuer une fonction de seuillage sur une image de type t_Image
 * seuil   Valeur de seuil
 * t_Image Pointeur sur l'image
*/
void outils_seuillage(unsigned int seuil, t_Image *img)
{
    unsigned int imValue;

    for (int i = 0; i < img->w; i++)
    {
        for (int j = 0; j < img->h; j++)
        {
            imValue = img->im[i][j];
            img->im[i][j] = (imValue < seuil) ? 0 : 255;
        }
    }
}

/**
 * Permet d'effectuer la différence entre deux images
 * img1   Pointeur sur l'image n°1
 * img2   Pointeur sur l'image n°2
 * Retourne la différence entrte les deux images
*/
t_Image* outils_diference(t_Image *img1, t_Image *img2)
{
    int imValue;
    t_Image *img3 = new(t_Image);

    img3->w = (img1->w > img2->w) ? img1->w : img2->w;
    img3->h = (img1->h > img2->h) ? img1->h : img2->h;

    for (int i = 0; i < TMAX; i++)
    {
        for (int j = 0; j < TMAX; j++)
        {
            imValue = img1->im[i][j] - img2->im[i][j];
            img3->im[i][j] = abs(imValue);
        }
    }

    return img3;
}