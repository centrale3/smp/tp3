using namespace std;

#include <stdio.h>
#include <string>
#include <iostream> //pour cout et cin
#include "image.h"
#include "chargesauve.h"
#include "outils.h"

int main()
{
    bool load;
    t_Image *lena = NULL;
    t_Image *diff = NULL;
    lena = new(t_Image);
    loadPgm("img/lena512x512.pgm", lena, load);

    if (!load)
        return -1;

    outils_seuillage(150, lena);
    diff = outils_diference(lena, lena);

    savePgm("img_out/lena_seuillee.png", lena);
    savePgm("img_out/lena_diff.png", diff);

    cout << "Chargement de l'image ok? " << load << endl;

    return 0;
}