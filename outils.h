#ifndef __OUTILS_H__
#define __OUTILS_H__

#include "image.h"

extern void outils_seuillage(unsigned int seuil, t_Image *img);
extern t_Image* outils_diference(t_Image *img1, t_Image *img2);

#endif